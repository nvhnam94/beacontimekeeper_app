//
//  LoginViewController.swift
//  BeaconTimekeeper
//
//  Created by BBaoBao on 10/16/16.
//  Copyright © 2016 12520274. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

  @IBOutlet weak var LBAppName: UILabel!
  @IBOutlet weak var TFUsername: UITextField!
  @IBOutlet weak var TFPassword: UITextField!
  @IBOutlet weak var BTLogin: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()
    self.configButtons()

  }

  func configButtons() {
    self.BTLogin.makeBorderAndCorner(width: 1, color: UIColor.white, corner: 5)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func btLoginTouch(_ sender: AnyObject) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let controller = storyboard.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
    self.present(controller, animated: true, completion: nil)
  }

}
