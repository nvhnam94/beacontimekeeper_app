//
//  ViewExtension.swift
//  BeaconTimekeeper
//
//  Created by BBaoBao on 10/16/16.
//  Copyright © 2016 12520274. All rights reserved.
//

import UIKit

extension UIView {
  func makeBorderAndCorner(width: CGFloat, color: UIColor, corner: CGFloat?=nil) {
    self.layer.borderWidth = width
    self.layer.borderColor = color.cgColor
    if let corner = corner {
      self.layer.cornerRadius = corner
    }
  }
}
